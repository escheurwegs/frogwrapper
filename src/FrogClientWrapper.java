package albatrosNLP;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

/**
 * This class attempts to launch a Frog server and communicate with it through a client
 * Calls the Frog NLP package by CLiPS (Antwerpen) and ILK Research Group (Tilburg)
 * Assumes frog is running as a server module; copy over the settings (e.g. skip modules) that you made launching the server
 *
 * - the function 'listener' will return an ArrayList of arrays that contains Folia XML, returned directly from frog.
 * - the function 'convertFoliaToTextItem' will convert this Folia to a TextWord object, based on the settings you have set here.
 * - the function 'process' will take a text and convert it to a TextWord object.
 * @author Elyne Scheurwegs
 * 
 * Tested on frog 0.12.19; 05 November 2014
 */
public class FrogClientWrapper {
	private static BufferedReader inp;
	private static BufferedWriter out;

	private static String frogHost = "localhost";		//host on which Frog is running
	private static int frogPort = 8020;					//port on which Frog is listening
	//Booleans indicating parts of Frog that are turned on/off (--skip=[mptncla])
	private static Boolean DEF_lemmatiser = true; 		//l
	private static Boolean DEF_morphanalyser = true;	//a
	private static Boolean DEF_chunker = true;			//c
	private static Boolean DEF_NER = true;				//n
	
	public FrogClientWrapper()
	{
		//Do initialisations if you want to load things from somewhere instead of working with hard-coded settings
	}
	
	public TextWord[] process(String text)
	{		
		ArrayList<String[]> frm = listener(text);
		return convertFoliaToTextItem(frm);
	}
	
	/**
	 * Launches a socket and waits for text to be returned
	 * @param text
	 * @return
	 */
	public ArrayList<String[]> listener(String text)
	{
		//Initialisations
		String markerEOT = "\r\nEOT\r\n";		//End of Text Marker that the FrogServer uses
		BufferedReader in;
		BufferedWriter out;
		Socket clientSocket;
		int timeOut = 120000;					//TimeOut in milliseconds
		long timeStart, timeLoop;
		ArrayList<String[]> data = new ArrayList<String[]>();
		String line;
		int size = -1;							//Check if returned content is always the same size

		// First, we trim our text so there are no trailing whitespaces
		// Java automatically uses unicode, so no conversion is needed for that
		text = text.trim();

		try
		{
			clientSocket = new Socket(frogHost, frogPort);
			clientSocket.setSendBufferSize(4096);
			clientSocket.setSoTimeout(timeOut);

			in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));

			// Writing text to output
			out.write(text);
			out.write(markerEOT);
			out.flush();

			timeStart = System.currentTimeMillis();
			do
			{
				line = in.readLine();
				if (line.trim().equals("READY") || line.trim().isEmpty())
				{
					// Ignoring empty lines and endmarker
				} else
				{
					// Processing line to a proper array
					String[] content = line.replace("\t\t","\t").split("\t");
					if (size > 0)
					{
						if (size == content.length)
						{
							//Content size is equal, add to data
							data.add(content);
						}
						else
						{
							System.err.println("A line returned by the server did not have the proper amount of columns!");
						}
					}
					else if (size == -1)
					{
						//Set first time
						size = content.length;
					}
					
					//System.out.println(StringUtils.join(content,","));
				}

				// Timeout marker
				timeLoop = System.currentTimeMillis();
			} while ((line.equals("READY") == false) && ((timeLoop - timeStart) < timeOut));

			//System.out.println("Finished");

			in.close();
			out.close();
			clientSocket.close();
		} catch (UnknownHostException e)
		{
			System.err.println("Unknown Host.");
			// System.exit(1);
		} catch (IOException e)
		{
			System.err.println("Couldn't get I/O for the connection.");
			e.printStackTrace();
			// System.exit(1);
		}

		return data;
	}
	
	
	private TextWord[] convertFoliaToTextItem(ArrayList<String[]> frm)
	{
		//Initialisations
		TextWord[] nti = new TextWord[frm.size()];
		int prevId = -1, lineId = 0;
		int currId;
		String word, lemma, morph[], tmp, pos, chunk, ner;
		/* standard matrix consists of 
		 * ID	woordvorm	lemma	morph		pos								certainty	ner		chunk	ignored	ignored
		 * 11,	linker,		linker,	[link][er],	ADJ(nom,basis,zonder,zonder-n),	0.500000,	O,		I-NP,	9,		obj1
		 */
		
		//Looping over matrix
		for (int i = 0; i < frm.size(); i++)
		{
			try
			{
				//Parsing Line
				String[] line = frm.get(i);
				//System.out.println(StringUtils.join(line,","));
				
				if (line.length > 3)
				{
					//First sorting out returning ID
					currId = Integer.parseInt(line[0]);
					if (prevId >= currId)
					{
						//increment sentence id
						lineId++;
					}
					prevId = currId;
					
					//Now parsing the present items
					word = line[1].trim();
					lemma = (DEF_lemmatiser) ? line[2].trim() : "";
					tmp = (DEF_morphanalyser) ? line[3].trim().replace("][", "£").replace("]", "").replace("[", "") : "";
					morph = tmp.split("£");
					pos = line[4];
					chunk = (DEF_chunker) ? line[7].trim() : "";
					ner = (DEF_NER) ? line[6].trim() : "";
					
					
					TextWord tw = new TextWord(lineId, currId, word, lemma, morph, pos,chunk,ner);
					nti[i] = tw;
				}
				else
				{
					System.err.println("The content you are trying to parse does not contain enough elements!");
				}
			}
			catch (NumberFormatException e)
			{
				System.err.println("No id assigned to this line!");
				TextWord tw = new TextWord(lineId, 0, "","", new String[1], "","","");
				nti[i] = tw;
			}
		}
 	
		return nti;
	}
}
