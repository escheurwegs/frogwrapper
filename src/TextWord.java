package albatrosNLP;

import java.util.HashMap;

import org.apache.commons.lang3.StringUtils;

/**
 * Contains a text chunk; This chunk consists of the (reconstruction) coordinates, the chunk content (often just a word), an hashmap with related features, 
 * its medical tag, its itemTag (containing classes like 'person'), its part-of-speech tag and certainty factor.
 * 
 * EDIT: toggled off all original id keepers, since they are not used for now
 * @author Elyne Scheurwegs 05 November 2014
 *
 */
public class TextWord {
	
	//Coördinates
	public int lineID;
	public int positionID;
	
	//public int origLineID;			// For reconstruction purposes
	//public int origPositionID;		// For reconstruction purposes
	//public String origWord;			// Original word value
	
	//Content
	public String word;			//Word value
	public String root;			//The word root detected by NLP Parsers
	public String[] split;			//The word parts detected by NLP Parsers
	
	//MetaData
	public String nerTag;
	public String chunkTag;
	public String posTag;
	public HashMap<String,Boolean> presentTags;	//Holds true for a certain tagCode
	public HashMap<String,Boolean> presetTags;		//Holds true when a tagCode was pre-detected
	public boolean preset;
	
	public boolean containsEscapedHTML;
	public double certainty;
	public HashMap<String, Double> features;				//Features (on a word level)
	
	

	public TextWord()
	{
		super();
		
		this.lineID = 0;
		this.positionID = 0;
		//this.origLineID = 0;
		//this.origPositionID = 0;
		//this.origWord = "";
		this.word = "";
		this.root = "";
		this.split = new String[1];
		this.nerTag = "";
		this.chunkTag = "";
		this.posTag = "";
		this.presentTags = new HashMap<>();
		this.presetTags = new HashMap<>();
		this.preset = false;
		this.containsEscapedHTML = false;
		this.certainty = 1;
		this.features = new HashMap<>();
	}
	
	public TextWord(String word)
	{
		super();
		
		this.lineID = 0;
		this.positionID = 0;
		//this.origLineID = 0;
		//this.origPositionID = 0;
		//this.origWord = "";
		this.word = word;
		this.root = "";
		this.split = new String[1];
		this.nerTag = "";
		this.chunkTag = "";
		this.posTag = "";
		this.presentTags = new HashMap<>();
		this.presetTags = new HashMap<>();
		this.preset = false;
		this.containsEscapedHTML = false;
		this.certainty = 1;
		this.features = new HashMap<>();
	}
	
	public TextWord(int lineID, int positionID, int origLineID, int origPositionID, String origWord, String word,
			String root, String[] split, String nerTag, String chunkTag, String posTag,
			HashMap<String, Boolean> presentTags, HashMap<String, Boolean> presetTags, boolean preset,
			boolean containsEscapedHTML, double certainty, HashMap<String, Double> features)
	{
		super();
		this.lineID = lineID;
		this.positionID = positionID;
		//this.origLineID = origLineID;
		//this.origPositionID = origPositionID;
		//this.origWord = origWord;
		this.word = word;
		this.root = root;
		this.split = split;
		this.nerTag = nerTag;
		this.chunkTag = chunkTag;
		this.posTag = posTag;
		this.presentTags = presentTags;
		this.presetTags = presetTags;
		this.preset = preset;
		this.containsEscapedHTML = containsEscapedHTML;
		this.certainty = certainty;
		this.features = features;
	}

	/**
	 * Constructor using the output from frog in initialisation
	 * @param lineId
	 * @param positionId
	 * @param content
	 * @param word
	 * @param root
	 * @param split
	 * @param posTag
	 * @param nerTag
	 * @param chunkTag
	 */
	public TextWord(int lineID, int positionID, String word, String root, String[] split, String posTag, String nerTag, String chunkTag)
	{
		super();
		this.lineID = lineID;
		this.positionID = positionID;
		this.word = word;
		this.root = root;
		this.split = split;
		this.nerTag = nerTag;
		this.chunkTag = chunkTag;
		this.posTag = posTag;
		
		this.presentTags = new HashMap<>();
		this.presetTags = new HashMap<>();
		this.preset = false;
		this.containsEscapedHTML = false;
		this.certainty = 1;
		this.features = new HashMap<>();
	}
	
	public int getLineID()
	{
		return lineID;
	}

	public void setLineID(int lineID)
	{
		this.lineID = lineID;
	}

	public int getPositionID()
	{
		return positionID;
	}

	public void setPositionID(int positionID)
	{
		this.positionID = positionID;
	}

	public String getWord()
	{
		return word;
	}

	public void setWord(String word)
	{
		this.word = word;
	}

	public String getRoot()
	{
		return root;
	}

	public void setRoot(String root)
	{
		this.root = root;
	}

	public String[] getSplit()
	{
		return split;
	}

	public void setSplit(String[] split)
	{
		this.split = split;
	}

	public String getNerTag()
	{
		return nerTag;
	}

	public void setNerTag(String nerTag)
	{
		this.nerTag = nerTag;
	}

	public String getChunkTag()
	{
		return chunkTag;
	}

	public void setChunkTag(String chunkTag)
	{
		this.chunkTag = chunkTag;
	}

	public HashMap<String, Boolean> getPresentTags()
	{
		return presentTags;
	}

	public void setPresentTags(HashMap<String, Boolean> presentTags)
	{
		this.presentTags = presentTags;
	}

	public HashMap<String, Boolean> getPresetTags()
	{
		return presetTags;
	}

	public void setPresetTags(HashMap<String, Boolean> presetTags)
	{
		this.presetTags = presetTags;
	}

	public boolean isPreset()
	{
		return preset;
	}

	public void setPreset(boolean preset)
	{
		this.preset = preset;
	}

	public boolean isContainsEscapedHTML()
	{
		return containsEscapedHTML;
	}

	public void setContainsEscapedHTML(boolean containsEscapedHTML)
	{
		this.containsEscapedHTML = containsEscapedHTML;
	}

	public double getCertainty()
	{
		return certainty;
	}

	public void setCertainty(double certainty)
	{
		this.certainty = certainty;
	}

	public HashMap<String, Double> getFeatures()
	{
		return features;
	}

	public String getPosTag()
	{
		return posTag;
	}

	public void setPosTag(String posTag)
	{
		this.posTag = posTag;
	}

	public void setFeatures(HashMap<String, Double> features)
	{
		this.features = features;
	}

	/**
	 * 
	 * @param key
	 * @param feat
	 */
	public void addFeature(String key, double feat)
	{
		this.features.put(key, feat);
	}
	
	/**
	 * 
	 * @param key
	 * @param value
	 * @param predetermined
	 */
	public void addTag(String key, boolean present, boolean predetermined)
	{
		this.presentTags.put(key,present);
		this.presetTags.put(key, predetermined);
	}

	public void print()
	{
		System.out.println(this.word + "," + this.getRoot() + "," + StringUtils.join(this.getSplit(),"|")+ "," + this.posTag + "," + this.chunkTag + "," + this.nerTag);
	}
}
